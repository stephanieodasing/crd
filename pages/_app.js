import "../styles/globals.scss";
import "../styles/admin.scss";
import Nav from "../components/layout/Nav";
import Header from "../components/layout/Header";
import Footer from "../components/layout/Footer"
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider } from "../context";

function MyApp({ Component, pageProps }) {
  return (
    <Provider>
      <ToastContainer position="top-right" closeOnClick autoClose={2000} />
      <Header />
      <Nav />
      <Component {...pageProps} />
      <Footer/>
    </Provider>
  );
}

export default MyApp;
