

export default function SecondaryDetails() {
    return (
        <>
            <div className="specialization-plan"><h5>Specialization Plan</h5></div>
            <h5 className="mt-3 mb-3">Secondary Details</h5>

            <label className="p-2" htmlFor="list-overview">List Overview:</label>
            <textarea className="form-control" id="listOverview"></textarea>
            <br />
            <label className="p-2" htmlFor="list-overview">Specialization Tags: <i>(seperate tags by comma)</i></label>
            <input className="form-control" type="text" id="specializationTags" />

            <label className="p-2" htmlFor="course-timeline-type">Course Timeline Type:</label>
            <select id="timelineType" className="form-select" aria-label="Default select example">
                <option defaultValue="1">Self-paced</option>
                <option defaultValue="2">Time-locked</option>
                <option defaultValue="3">Continuous</option>
            </select>
            <br />
            <div className="type-details">
                <b>Self-paced Courses</b> allow the student to complete the course anytime. {"\n"}<b>Time-locked Courses</b> archives the course after a set period of time. {"\n"}<b>Continuous Course</b>s are courses that can stop for a set period of time then restart again. {"\n"}You selected <b>Self-paced</b>.
            </div>

            <div className="save-details">
                <button type="button" className="btn btn-light mb-2 mt-3">Save Details</button>
            </div>

            <style jsx>{`

                .type-details {
                    white-space: pre-wrap;
                    background: #C4C4C4;
                    border-radius: 20px;
                    padding: 10px 10px;
                }

                .specialization-plan {
                    background: #000;
                    color: #fff;
                    padding: 10px 0;
                    width: 75%;
                }

                .btn {
                    background-color: #87CEEB; 
                    width: 386px;
                    height: 74pxpx;
                    border-radius: 10px;
                    margin-left: auto; 
                    margin-right: auto;
                    display: block;
                }
                
                `}
            </style>

        </>
    )
}