import Image from "next/image";
import Upload from "../../public/images/Admin/Vector.png";
import Link from "next/link";

export default function MainDetails() {
  function saveDetails() {
    alert("Details Saved!");
  }

  return (
    <>
      <div className="specialization-content">
        <h5>Specialization Content</h5>
      </div>

      <h5 className="mt-3 p-2">
        Course Content: <span className="float-end fw-bold">Course 1 of 4</span>
      </h5>

      <div className="row">
        <div className="col-6">
          <button className="btnDetails fw-bold">Course Details</button>
        </div>
        <div className="col-6">
          <button className="btnCurriculum fw-bold">Curriculum</button>
        </div>
      </div>

      <div className="course-body">
        <div className="course-main-body">Week 1</div>

        <label htmlFor="chapter-title">Chapter Title</label>
        <input className="form-control" type="text" id="chapterTitle" />

        <label htmlFor="chapter-summary">Chapter Summary</label>
        <textarea className="form-control" id="chapterSummary"></textarea>
      </div>

      <div className="row mt-3 mb-3">
          <div className="col-6">
            <button type="button" className="btnAdd fw-bold">
                ADD ANOTHER WEEK
            </button>
          </div>
          <div className="col-6">
            <button type="button" className="btnProceed fw-bold">
                PROCEED COURSE 2
            </button>
          </div>
      </div>

      <style jsx>
        {`
          .specialization-content {
            background: #000;
            color: #fff;
            padding: 10px 0;
            width: 75%;
          }

          .btnAdd {
            background-color: #C4C4C4;
            width: 80%;
            border-radius: 10px;
            margin-left: auto;
            margin-right: auto;
            display: block;
            border: none;
          }

          .btnProceed {
            background-color: #87CEEB;
            width: 80%;
            border-radius: 10px;
            margin-left: auto;
            margin-right: auto;
            display: block;
            border: none;
          }

          .btnDetails {
            border-radius: 10px;
            width: 100%;
            background: #c4c4c4;
            border: none;
          }

          .btnCurriculum {
            border-radius: 10px;
            width: 100%;
            background: #ffffff;
            border: 1px solid;
          }

          .course-body {
            border: 1px solid;
            border-radius: 20px;
            padding: 10px 10px 10px 10px;
          }

          .course-main-body {
                background: #C4C4C4;
                border-radius: 20px;
            }
        `}
      </style>
    </>
  );
}
