import DraggableItem from './DraggableItem'
import data from './sample_data.json'

export default function LessonDetail(){
  return(
    <DraggableItem
      data = {data.lesson_1}
    />
  )
}