import DraggableItem from './DraggableItem'
import data from './sample_data.json'

export default function CurriculumCourses(){
  return(
    <DraggableItem
      data = {data.courses}
    />
  )
}