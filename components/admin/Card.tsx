import Image from "next/image";
import Specialization1 from "../../public/images/Admin/Specialization1.png";
import FreeStanding from "../../public/images/Admin/freestanding.png";

export default function WelcomeCourseCreator() {
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="cards">
              <div className="cards-image">
                <Image
                  src={Specialization1}
                  layout="responsive"
                  sizes="100vw"
                  placeholder="blur"
                  width={100}
                  height={100}
                />
              </div>
              <div className="cards-label">Create Specialization</div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="cards">
              <div className="cards-image">
                <Image
                  src={FreeStanding}
                  layout="responsive"
                  sizes="100vw"
                  placeholder="blur"
                  width={100}
                  height={100}
                />
              </div>
              <div className="cards-label">Create Free-Standing</div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="card-background">
              <div className="card-foreground">
                <div className="welcome-card">
                  WELCOME TO COURSE CREATOR LABS!
                </div>
                <br />
                <div className="body-card">
                  <p>
                    There are two ways to create a course.
                    <br />
                    <br />
                    <b>1. Create Specialization</b>
                    <br />
                    Do you have a set of courses that will help a student become an expert on a particular subject? Select this option as it allows you to put all these courses into one.
                    <br />
                    <br />
                    <b>2. Create Free-standing</b>
                    <br />
                    Is your course a stand-alone? Does your course not need many courses in one? If your answer is yes, then, choose this course.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <style jsx>{`
        .cards {
          width: 90%;
          margin-left: auto;
          margin-right: auto;
        }

        .cards-image {
          background: #808080;
          padding: 20px 10px;
          border-radius: 30px;
        }

        .cards-label {
          background: #87ceeb;
          font-size: 43px;
          text-align: center;
          padding: 20px 10px;
          border-radius: 20px;
          transform: translateY(-15px);
        }

        .card-background {
          background-image: linear-gradient(109deg, skyblue 60%, white 60.5%);
          border-radius: 50px;
          width: 100%;
          text-align: right;
        }

        .card-foreground {
          border: 1px solid black;
          border-radius: 50px;
          background: #ffffff;
          padding: 10px 10px;
          margin-left: auto;
          width: 95%;
        }

        .welcome-card {
          background: #000000;
          color: #ffffff;
          transform: translateX(-20px);
          padding: 5px 10px;
          width: 80%;
          font-size: 35px;
          text-align: center;
        }

        p {
          font-size: 40px;
          text-align: left;
          padding-left: 10px;
        }
      `}</style>
    </>
  );
}
