
export default function RelatedCourses() {
    return (
        <>
            
                <h3>RELATED COURSES</h3>
                <div className="row">
                        <div className="col-sm-4">
                            <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/434341/pexels-photo-434341.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=5000"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.9</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                            </div>
                            <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/159581/dictionary-reference-book-learning-meaning-159581.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.5</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                            </div>
                            <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/448835/pexels-photo-448835.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.3</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/415071/pexels-photo-415071.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.1</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                            </div>
                            <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/256541/pexels-photo-256541.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.5</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                            </div>
                            <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/2249063/pexels-photo-2249063.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.9</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/3431444/pexels-photo-3431444.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 5.0</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                        </div>
                        <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/3769708/pexels-photo-3769708.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.0</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                        </div>
                        <div className="d-flex related-courses">
                            {/* <!-- Image --> */}
                            <img
                                src="https://images.pexels.com/photos/5984614/pexels-photo-5984614.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                                className="me-3"
                            />
                            {/* <!-- Body --> */}
                            <div>
                                <h5 className="fw-bold">Course Title</h5>
                                <small><strong><i className="fas fa-star colorMeOrange"></i> 4.7</strong></small><br />
                                <small className="text-muted">Basic information: 4 weeks</small><br />
                                <small className="text-muted">Duration: 5 above</small><br />
                                <small className="text-muted">Applicable Levels: </small>
                            </div>
                        </div>
                    </div>
                </div>
        </>
    )
}